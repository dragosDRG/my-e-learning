<?php

namespace App\Controller;

use App\Entity\Account;
use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\Cours;
use App\Entity\Roles;
use App\Repository\AccountRepository;
use App\Repository\CategoryRepository;
use App\Repository\CommentRepository;
use App\Repository\CoursRepository;
use App\Repository\RolesRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /** @var Session */
    public $sesion;

    /**
     * HomeController constructor.
     */
    public function __construct()
    {
        $this->sesion = new Session();
    }

    /**
     * @Route("/", name = "home")
     *
     * @return Response
     */
    public function homeAction()
    {
        return new Response(
            $this->renderView(
                'home.html.twig',
                [
                    'title' => 'e-learning',
                    'phone_number' => '0761511179',
                    'session' => $this->sesion
                ]
            )
        );
    }

    /**
     * @Route("/about", name = "about")
     *
     * @return Response
     */
    public function aboutAction()
    {
        return new Response(
            $this->renderView(
                'about.html.twig'
            )
        );
    }

    /**
     * @Route("/single-courses/{teacher}", name ="singleCourses")
     *
     * @return Response
     */
    public function coursesAction($teacher)
    {
        $title = 'The Unreal Engine Developer Course Learn C++ & Make Games';
        $email = 'test@test.com';
        $phoneNumber = '07611179';
        $teacherName = $teacher;
        $categoryName = 'CategoryTest';
        $categoryNameTitle = 'PHP WEB design';
        $descriptionTitle = 'What is learn?';
        $description = 'Learn C++, the games industry standard language.
                        Develop strong and transferrable problem solving skills.
                        Gain an excellent knowledge of modern game development.
                        Learn how object oriented programming works in practice.
                        Gain a more fundamental understanding of computer operation.';
        $contentCouseTitle = 'Content';
        $contentOfCouses = "EW Testing Grounds FPS shipped, including...
                    Much more C++ and Blueprint.
                    AI Blackboards & Behavior Trees.
                    Environmental Query System (EQS).
                    Humanoid Animation Blending.
                    Never-ending Level Design.
                    The course now has high quality hand written subtitles throughout, available as closed captions
                     you can turn them on or off at your convenience.
                    This course started as a runaway success on Kickstarter. Get involved now, and get access to all
                    future content as it\'s added. The final course will be over 50 hours of content and 300+
                    lectures.
                    Learn how to create and mod video games using Unreal Engine 4, the free-to-use game development
                        platform used by AAA studios and indie developers worldwide.
                    We start super simple so you need no prior experience of Unreal or coding! With our online
                        tutorials, you\'ll be amazed what you can achieve.
                    Benefit from our world-class support from both other students, and the instructors who are on the
                        forums regularly. Go on to build several games including a tank game, and a First Person
                        Shooter.
                    You will have access to a course forum where you can discuss topics on a course-wide basis, or
                        down to the individual video. Our thriving discussion forum will help you learn and share ideas
                        with other students.
                    You will learn C++, the powerful industry standard language from scratch. By the end of the
                        course you\'ll be very confident in the basics of coding and game development, and hungry to
                        learn more.
                    'Any serious game programmer needs to know C++'Jason Gregory, Lead Programmer at Naughty Dog
                        (creators of Uncharted & The Last of Us)
                    Anyone who wants to learn to create games: Unreal Engine is a fantastic platform which enables
                        you to make AAA-quality games. Furthermore these games can be created for Windows, consoles,
                        MacOS, iOS, Android and Web from a single source!
                    If you\'re a complete beginner, we\'ll teach you all the coding and game design principles you\'ll
                        need. If you\'re an artist, we\'ll teach you to bring your assets to life. If you\'re a coder,
                        we\'ll teach you game design principles.
                    What this course DOESN\'T cover...
                    Whereas this course is already huge, we can\'t possibly cover everything in that time. Here are
                    some things we will not be covering..";
        return new Response(
            $this->renderView(
                'single-courses.html.twig', [
                    'votes' => 2,
                    'email' => $email,
                    'phoneNumber' => $phoneNumber,
                    'title' => $title,
                    'teacherName' => $teacherName,
                    'categoryName' => $categoryName,
                    'categoryNameTitle' => $categoryNameTitle,
                    'descriptionTitle' => $descriptionTitle,
                    'description' => $description,
                    'contentOfCouses' => $contentOfCouses,
                    'contentCouseTitle' => $contentCouseTitle
                ]
            )
        );
    }

    /**
     * @Route("/contact", name = "contact")
     *
     * @return Response
     */
    public function contactAction()
    {
        return new Response(
            $this->renderView(
                'contact.html.twig'
            )
        );
    }

    /**
     * @Route("/single-post/{teacherId}/{courseName}", name = "simplePost")
     *
     * @return Response
     */
    public function simplePostAction($teacherId, $courseName)
    {
        $entityManager = $this->getDoctrine()->getManager();

        /** @var AccountRepository $accountRepository */
        $accountRepository = $entityManager->getRepository(Account::class);

        /** @var RolesRepository $roleRepository */
        $roleRepository = $entityManager->getRepository(Roles::class);

        /** @var CommentRepository $commentRepository */
        $commentRepository = $entityManager->getRepository(Comment::class);

        /** @var CoursRepository $course */
        $coursRepository = $entityManager->getRepository(Cours::class);

        /** @var Account $teacherEntity */
        $teacherEntity = $accountRepository->findOneBy([
            'id' => $teacherId,
            'status' => Account::STATUS_ACTIV
        ]);

        /** @var Roles $role */
        $role = $roleRepository->findOneBy([
            'id' => $teacherEntity->getRoleId(),
            'status' => Roles::STATUS_ACTIVE
        ]);

        /** @var Cours $course */
        $course = $coursRepository->findOneBy([
            'name' => $courseName,
            'status' => Cours::STATUS_ACTIVE,
            'accountId' => $teacherId,
        ]);
        if ($course instanceof Cours) {
            /** @var array $comments */
            $comments = $commentRepository->findBy([
                'userId' => $teacherEntity->getId(),
                'status' => Comment::STATUS_ACTIV,
                'courseId' => $course->getId()
            ]);
        }

        return new Response(
            $this->renderView(
                'single-post.html.twig',
                [
                    'teacher' => $teacherEntity,
                    'course' => $course,
                    'role' => $role,
                    'comments' => $comments
                ]
            )
        );
    }

    /**
     * @Route("/courses/{teacher}", name = "courses")
     *
     * @param $teacher
     *
     * @return Response
     */
    public function coursesWithTeacher($teacher)
    {
        return new Response(
            $this->renderView(
                'courses.html.twig', [
                    'teacherCourses' => ['testCurs_1', 'testCurs_2', 'testCurs_3', 'testCurs_4'],
                    'votes' => 2,
                    'email' => $teacher . '@yahoo.com',
                    'phoneNumber' => '07611123555',
                    'title' => 'Cursuri si materiale :',
                    'teacherName' => $teacher,
                    'categoryName' => 'test',
                    'categoryNameTitle' => 'test',
                    'descriptionTitle' => 'test',
                    'description' => "'bl'bla'bla'",
                    'contentOfCouses' => $teacher,
                    'contentCouseTitle' => $teacher
                ]
            )
        );
    }

    /**
     * @Route("/addCategory/{name}",name="category")
     */
    public function addCategoryAction($name)
    {
        $category = new Category();
        $category->setName($name)
            ->setStatus(Category::STATUS_ACTIVE);

        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->persist($category);
        $entityManager->flush();

        return new Response('Saved new product with id ' . $category->getId());
    }

    /**
     * @return Response
     * @Route("/login", name="login")
     */
    public function login()
    {
        return new Response(
            $this->renderView(
                'login.html.twig'
            )
        );
    }

    /**
     * @Route("/register", name = "register")
     *
     * @param Request $request
     *
     * @return Response
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function register()
    {
        return new Response(
            $this->renderView(
                'register.html.twig'
            )
        );
    }

    /**
     * @Route("/add-new-account", name = "addNewAcount")
     *
     * @param Request $request
     *
     * @return Response
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addNewAccount(Request $request)
    {
        $email = $request->request->get('email');
        $password = $request->request->get('password');
        $firstNames = $request->request->get('firstNames');
        $secondName = $request->request->get('secondName');
        $descriere = $request->request->get('descriere');
        $profesie = $request->request->get('profesie');

        /** @var EntityManager $entityManager */
        $entityManager = $this->getDoctrine()->getManager();

        $account = new Account();
        $account->setStatus(1)
            ->setRoleId(Roles::ADMINISTRATOR_ID)
            ->setDescription($descriere)
            ->setFirstName($firstNames)
            ->setSecondName($secondName)
            ->setProfesie($profesie)
            ->setEmail($email)
            ->setPassword(md5($password))
            ->setVotes(0);

        $entityManager->persist($account);
        $entityManager->flush();

        $this->sesion->clear();
        $this->sesion->set('email', $email);

        return new JsonResponse('success');
    }

    /**
     *
     * @Route("/teachers", name = "teachers")
     *
     * @return Response
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function renderTeachers()
    {
        try {
            /** @var EntityManager $entityManager */
            $entityManager = $this->getDoctrine()->getManager();
            $accountRepository = $entityManager->getRepository(Account::class);
            $coursRepository = $entityManager->getRepository(Cours::class);
            $categoryRepository = $entityManager->getRepository(Category::class);

            $teachersEntity = $accountRepository->findBy(['status' => Account::STATUS_ACTIV]);
            $categorys = $categoryRepository->findBy(['status' => Account::STATUS_ACTIV]);

            /** @var Account $teacherEntity */
            foreach ($teachersEntity as $teacherEntity) {
                $teacherNames[] = $teacherEntity->getSecondName();
                $courses[$teacherEntity->getSecondName()] = $coursRepository->findBy([
                    'accountId' => $teacherEntity->getId(),
                    'status' => Account::STATUS_ACTIV
                ]);

                /** @var Category $category */
                foreach ($categorys as $category) {
                    $categoryss[$teacherEntity->getSecondName()][] = $category->getName();
                }
            }

            return new Response(
                $this->renderView(
                    'teachers.html.twig', [
                        'title' => 'E-learning platform',
                        'teacherNames' => $teachersEntity,
                        'categorys' => $categoryss,
                        'courses' => $courses
                    ]
                )
            );
        } catch (\Exception $exception) {
            throw new \Exception("A aparut o eroare in cursul de cautare . Incearca mai tarziu");
        }
    }

    /**
     *
     * @Route("/cursuri-online", name = "onlineCourses")
     *
     * @return Response
     *
     */
    public function onlineCourses()
    {
        return new Response(
            $this->renderView(
                'onlineCourses.html.twig', [
                    'title' => 'E-learning platform'
                ]
            )
        );
    }

    /**
     *
     * @Route("/cursuri/{teacherId}", name = "cursuriProfesori")
     *
     * @return Response
     *
     */
    public function cousesForAnyTeacher($teacherId)
    {
        /** @var integer $incrementCourse */
        $incrementCourse = 0;

        /** @var array $courses */
        $courses = [];

        /** @var EntityManager $entityManager */
        $entityManager = $this->getDoctrine()->getManager();
        $accountRepository = $entityManager->getRepository(Account::class);
        $coursRepository = $entityManager->getRepository(Cours::class);
        $categoryRepository = $entityManager->getRepository(Category::class);

        /** @var Account $teacherEntity */
        $teacherEntity = $accountRepository->findOneBy([
            'id' => $teacherId,
            'status' => Account::STATUS_ACTIV
        ]);

        $categorys = $categoryRepository->findBy(['status' => Account::STATUS_ACTIV]);
        /** @var Category $category */
        foreach ($categorys as $category) {
            $categoryss[$teacherEntity->getSecondName()][] = $category->getName();
            $coursesEntity = $coursRepository->findBy([
                'categoryId' => $category->getId(),
                'accountId' => $teacherEntity->getId(),
                'status' => Cours::STATUS_ACTIVE
            ]);
            $courses[$category->getName()] = [];
            /** @var Cours $courseEntity */
            foreach ($coursesEntity as $courseEntity) {
                $courses[$category->getName()][] = $courseEntity;
                $incrementCourse++;
            }
        }

        return new Response(
            $this->renderView(
                'coursesForAnyTeacher.html.twig', [
                    'title' => 'E-learning platform',
                    'teacher' => $teacherEntity,
                    'courses' => $courses,
                    'categorys' => $categoryss,
                    'descriptionTeacher' => $teacherEntity->getDescription(),
                    'profesie' => $teacherEntity->getProfesie(),
                    'votes' => $teacherEntity->getVotes(),
                    'totalNumberCourses' => $incrementCourse,
                    'coursesEntity' => $coursesEntity
                ]
            )
        );
    }

    /**
     * @Route("/save-comment", name = "saveComment")
     *
     * @param Request $request
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public
    function saveComment(Request $request)
    {
        $this->sesion->set('username', 'dragos.puscu');

        /** @var EntityManager $entityManager */
        $entityManager = $this->getDoctrine()->getManager();

        $name = $request->request->get('name');
        $title = $request->request->get('title');
        $comment = $request->request->get('comment');
        $userId = $request->request->get('userId');
        $courseId = $request->request->get('courseId');

        $commentEntity = new Comment();
        $commentEntity->setContent($comment)
            ->setTitle($title)
            ->setUserComment($name)
            ->setUserId($userId)
            ->setCourseId($courseId)
            ->setStatus(1);

        $entityManager->persist($commentEntity);
        $entityManager->flush();
        if ($commentEntity instanceof Comment) {
            return new JsonResponse('Multumim pentru comentariu. Orice sugestie ne ajuta.');
        } else {
            return new JsonResponse('Ne pare rau a aparut o probema in salvarea sugestiilor tale. Te rog incearca din nou mai tarziu.');
        }

    }

    /**
     * @Route("/check-auth", name = "checkAuth")
     *
     * @param Request $request
     */
    public function checkAuth(Request $request)
    {
        $email = $request->request->get('email');
        $pasword = $request->request->get('pasword');

        /** @var EntityManager $entityManager */
        $entityManager = $this->getDoctrine()->getManager();

        /** @var AccountRepository $accountRepository */
        $accountRepository = $entityManager->getRepository(Account::class);

        /** @var Account $account */
        $account = $accountRepository->findOneBy(
            [
                'email' => $email,
                'password' => md5($pasword),
                'status' => Account::STATUS_ACTIV
            ]
        );

        if ($account instanceof Account && $account->getPassword() == md5($pasword)) {
            $this->sesion->set('email', $email);
            return new JsonResponse(['error' => false]);
        }
        $this->sesion->clear();
        return new JsonResponse(
            [
                'error' => true,
                'message' => "Parola introdusa sau email-ul sunt gresite . Te rog reincearca cu mai multa  atentie!"
            ]);

    }

    /**
     * @Route("/administrare", name = "administrare")
     */
    public function administrare()
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getDoctrine()->getManager();

        /** @var AccountRepository $accountRepository */
        $accountRepository = $entityManager->getRepository(Account::class);

        /** @var CoursRepository $coursesRepository */
        $coursesRepository = $entityManager->getRepository(Cours::class);

        /** @var CommentRepository $commentRepository */
        $commentRepository = $entityManager->getRepository(Comment::class);

        /** @var CategoryRepository $categoryRepository */
        $categoryRepository = $entityManager->getRepository(Category::class);

        /** @var Account $account */
        $account = $accountRepository
            ->findOneBy(
                [
                    'email' => $this->sesion->get('email'),
                    'status' => Account::STATUS_ACTIV,
                ]
            );

        $courses = $coursesRepository->findBy(
            [
                'accountId' => $account->getId(),
                'status' => Cours::STATUS_ACTIVE
            ]
        );

        $comments = $commentRepository->findBy(
            [
                'userId' => $account->getId(),
                'status' => Cours::STATUS_ACTIVE
            ]
        );

        $categorys = $categoryRepository->findBy([
                'status' => Category::STATUS_ACTIVE
            ]
        );


        return new Response(
            $this->renderView(
                'administrare.html.twig', [
                    'title' => 'E-learning platform',
                    'teacher' => $account,
                    'courses' => $courses,
                    'categorys' => $categorys,
                    'totalCourses' => count($courses),
                    'numberComment' => count($comments)
                ]
            )
        );
    }

    /**
     * @Route("/delete-courses", name = "deleteCourses")
     */
    public function deleteCourses(Request $request)
    {
        /** @var integer $courseId */
        $courseId = $request->request->get('courseId');

        /** @var EntityManager $entityManager */
        $entityManager = $this->getDoctrine()->getManager();

        /** @var CoursRepository $coursesRepository */
        $coursesRepository = $entityManager->getRepository(Cours::class);

        /** @var Cours $course */
        $course = $coursesRepository->find($courseId);

        if ($course instanceof Cours) {
            $course->setStatus(Cours::STATUS_INACTIVE);

            $entityManager->persist($course);
            $entityManager->flush();
        }
        return new JsonResponse('Curs sters cu success');
    }

    /**
     * @Route("/like", name = "like")
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function likeTeacher(Request $request)
    {
        $teacherId = $request->request->get('teacherId');

        /** @var EntityManager $entityManager */
        $entityManager = $this->getDoctrine()->getManager();

        /** @var AccountRepository $accountRepository */
        $accountRepository = $entityManager->getRepository(Account::class);

        /** @var Account $account */
        $account = $accountRepository->find($teacherId);

        /** @var integer $like */
        $like = $account->getVotes() + 1;

        if ($account instanceof Account) {
            $account->setVotes($like);

            $entityManager->persist($account);
            $entityManager->flush();

            return new JsonResponse('Multumesc pentru apreciere');
        }
        return new JsonResponse('A aparut o eroare te rog reincearca mai tarziu!');
    }

    /**
     * @Route("/logout", name = "logout")
     */
    public function logout()
    {
        $this->sesion->clear();

        return $this->homeAction();
    }

    /**
     * @param Request $request
     *
     * @Route("/add-new-course", name = "addNewCourse")
     *
     * @return JsonResponse
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addNewCourse(Request $request, $uploadDirectory)
    {
        $teacherId = $request->request->get('teacher');
        $categoryId = $request->request->get('category');
        $name = $request->request->get('nameCourse');
        $content = $request->request->get('content');

        /** @var EntityManager $entityManager */
        $entityManager = $this->getDoctrine()->getManager();

        /** @var AccountRepository $accountRepository */
        $accountRepository = $entityManager->getRepository(Account::class);

        $file = $request->files->get('myfile');
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        try {
            $file->move(
                $uploadDirectory,
                $originalFilename . '.pdf'
            );
        } catch (FileException $e) {
        }

        $course = new Cours();
        $course->setStatus(Roles::STATUS_ACTIVE)
            ->setAccountId($teacherId)
            ->setCategoryId($categoryId)
            ->setName($name)
            ->setPath($originalFilename . '.pdf')
            ->setContent($content);

        $entityManager->persist($course);
        $entityManager->flush();

        return new JsonResponse('Fisier adaugat cu success');
    }

    /**
     * @param Request $request
     *
     * @Route("/learn-with-champions", name = "learnWithChampions")
     *
     */
    public function learnWithChampions()
    {
        try {
            /** @var EntityManager $entityManager */
            $entityManager = $this->getDoctrine()->getManager();
            $accountRepository = $entityManager->getRepository(Account::class);
            $coursRepository = $entityManager->getRepository(Cours::class);
            $categoryRepository = $entityManager->getRepository(Category::class);

            $teachersEntity = $accountRepository->findBy(['status' => Account::STATUS_ACTIV]);
            $categorys = $categoryRepository->findBy(['status' => Account::STATUS_ACTIV]);

            /** @var Account $teacherEntity */
            foreach ($teachersEntity as $teacherEntity) {
                $teacherNames[] = $teacherEntity->getSecondName();
                $courses[$teacherEntity->getSecondName()] = $coursRepository->findBy([
                    'accountId' => $teacherEntity->getId(),
                    'status' => Account::STATUS_ACTIV
                ]);

                /** @var Category $category */
                foreach ($categorys as $category) {
                    $categoryss[$teacherEntity->getSecondName()][] = $category->getName();
                }
            }

            return new Response(
                $this->renderView(
                    'teachers.html.twig', [
                        'title' => 'E-learning platform',
                        'teacherNames' => $teachersEntity,
                        'categorys' => $categoryss,
                        'courses' => $courses
                    ]
                )
            );
        } catch (\Exception $exception) {
            throw new \Exception("A aparut o eroare in cursul de cautare . Incearca mai tarziu");
        }
    }

    /**
     * @Route("/download-all-courses", name = "downloadAllCourses")
     */
    public function downloadAllCourses()
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getDoctrine()->getManager();

        /** @var CoursRepository $coursRepository */
        $coursRepository = $entityManager->getRepository(Cours::class);
        try {
            $courses = $coursRepository->findBy([
                'status' => Account::STATUS_ACTIV
            ]);
            if (count($courses) > 0) {
                return new Response(
                    $this->renderView(
                        'downloadAll-Courses.html.twig', [
                            'title' => 'E-learning platform',
                            'courses' => $courses,
                            'totalCourses' => count($courses)
                        ]
                    )
                );
            } else {
                throw new \Exception("A aparut o problema in cautarea cursurilor si materialelor");
            }
        } catch (\Exception $exception) {
            return new Response(
                $this->renderView('error-download-courses.html.twig',
                    [
                        'title' => 'E-learning platform',
                        'error' => $exception->getMessage(),
                    ])
            );
        }

    }
}