<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Category
 *
 * @ORM\Table(name="account")
 * @ORM\Entity
 *
 */
class Account
{
    public const STATUS_ACTIV = 1;
    public const STATUS_INACTIV = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="roles_id", type="integer", nullable=false)
     *
     */
    private $roleId;

    /**
     * @var int
     *
     * @ORM\Column(name="votes", type="integer", nullable=false)
     *
     */
    private $votes;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", nullable=false)
     *
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", nullable=false)
     *
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", nullable=false)
     *
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="second_name", type="string", nullable=false)
     *
     */
    private $secondName;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", nullable=false)
     *
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="profesie", type="string", nullable=false)
     *
     */
    private $profesie;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     *
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", nullable=false)
     *
     */

    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     *
     */
    private $modified;

    /**
     * @var string
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     *
     */
    private $created;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Account
     */
    public function setId(int $id): Account
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getRoleId(): int
    {
        return $this->roleId;
    }

    /**
     * @param int $roleId
     * @return Account
     */
    public function setRoleId(int $roleId): Account
    {
        $this->roleId = $roleId;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return Account
     */
    public function setPassword(string $password): Account
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Account
     */
    public function setEmail(string $email): Account
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return Account
     */
    public function setFirstName(string $firstName): Account
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getSecondName(): string
    {
        return $this->secondName;
    }

    /**
     * @param string $secondName
     * @return Account
     */
    public function setSecondName(string $secondName): Account
    {
        $this->secondName = $secondName;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Account
     */
    public function setDescription(string $description): Account
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getProfesie(): string
    {
        return $this->profesie;
    }

    /**
     * @param string $profesie
     * @return Account
     */
    public function setProfesie(string $profesie): Account
    {
        $this->profesie = $profesie;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return Account
     */
    public function setStatus(string $status): Account
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getModified(): string
    {
        return $this->modified;
    }

    /**
     * @param string $modified
     * @return Account
     */
    public function setModified(string $modified): Account
    {
        $this->modified = $modified;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreated(): string
    {
        return $this->created;
    }

    /**
     * @param string $created
     * @return Account
     */
    public function setCreated(string $created): Account
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return int
     */
    public function getVotes(): int
    {
        return $this->votes;
    }

    /**
     * @param int $votes
     * @return Account
     */
    public function setVotes(int $votes): Account
    {
        $this->votes = $votes;
        return $this;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @param string $image
     * @return Account
     */
    public function setImage(string $image): Account
    {
        $this->image = $image;
        return $this;
    }
}