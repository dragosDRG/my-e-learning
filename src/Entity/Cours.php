<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Category
 *
 * @ORM\Table(name="cours")
 * @ORM\Entity
 *
 */
class Cours
{
    /** @const int */
    const STATUS_ACTIVE = 1;

    /** @const int */
    const STATUS_INACTIVE = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="category_id", type="integer", nullable=false)
     *
     */
    private $categoryId;

    /**
     * @var int
     *
     * @ORM\Column(name="account_id", type="integer", nullable=false)
     *
     */
    private $accountId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=false)
     *
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", nullable=false)
     *
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="string", nullable=false)
     *
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     *
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     *
     */
    private $modified;

    /**
     * @var string
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     *
     */
    private $created;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Cours
     */
    public function setId(int $id): Cours
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getCategoryId(): string
    {
        return $this->categoryId;
    }

    /**
     * @param string $categoryId
     * @return Cours
     */
    public function setCategoryId(string $categoryId): Cours
    {
        $this->categoryId = $categoryId;
        return $this;
    }

    /**
     * @return int
     */
    public function getAccountId(): int
    {
        return $this->accountId;
    }

    /**
     * @param int $accountId
     * @return Cours
     */
    public function setAccountId(int $accountId): Cours
    {
        $this->accountId = $accountId;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Cours
     */
    public function setName(string $name): Cours
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return Cours
     */
    public function setContent(string $content): Cours
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return Cours
     */
    public function setStatus(string $status): Cours
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getModified(): string
    {
        return $this->modified;
    }

    /**
     * @param string $modified
     * @return Cours
     */
    public function setModified(string $modified): Cours
    {
        $this->modified = $modified;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreated(): string
    {
        return $this->created;
    }

    /**
     * @param string $created
     * @return Cours
     */
    public function setCreated(string $created): Cours
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     * @return Cours
     */
    public function setPath(string $path): Cours
    {
        $this->path = $path;
        return $this;
    }
}