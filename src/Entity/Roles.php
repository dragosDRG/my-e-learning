<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Category
 *
 * @ORM\Table(name="roles")
 * @ORM\Entity
 *
 */
class Roles
{
    /** @const int */
    const STATUS_ACTIVE = 1;

    /** @const int */
    const STATUS_INACTIVE = 0;

    /** @var string  */
    const ADMINISTRATOR_ID = '1';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=false)
     *
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     *
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     *
     */
    private $modified;

    /**
     * @var string
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     *
     */
    private $created;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Roles
     */
    public function setId(int $id): Roles
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Roles
     */
    public function setName(string $name): Roles
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return Roles
     */
    public function setStatus(string $status): Roles
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getModified(): string
    {
        return $this->modified;
    }

    /**
     * @param string $modified
     * @return Roles
     */
    public function setModified(string $modified): Roles
    {
        $this->modified = $modified;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreated(): string
    {
        return $this->created;
    }

    /**
     * @param string $created
     * @return Roles
     */
    public function setCreated(string $created): Roles
    {
        $this->created = $created;
        return $this;
    }
}