function about() {
    jQuery.ajax({
        type: "POST",
        url: '/about',
        cache: false,
        success: function (data) {
            if (!data.error) {
                jQuery('#subject').html(data);
            }
        }
    });
}