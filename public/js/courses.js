function courses() {
    jQuery.ajax({
        type: "POST",
        url: '/single-courses',
        cache: false,
        success: function (data) {
            if (!data.error) {
                jQuery('#home_full_page').html(data);
            }
        }
    });
}

function uploadFile() {
    var $data = new FormData(document.querySelector('#add-course-form'));

    jQuery.ajax({
        type: "POST",
        url: '/add-new-course',
        processData: false,
        contentType: false,
        data: $data,
        success: function (data) {
            alert(data);
            location.reload();
        }
    });

    return false;
}

function deleteCourses(courseId) {
    var $data = {courseId: courseId};

    jQuery.ajax({
        type: "POST",
        url: '/delete-courses',
        cache: false,
        data: $data,
        success: function (data) {
            alert(data);
            location.reload();
        }
    });
}