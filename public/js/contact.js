function contact() {
    jQuery.ajax({
        type: "POST",
        url: '/contact',
        cache: false,
        success: function (data) {
            if (!data.error) {
                jQuery('#subject').html(data);
            }
        }
    });
}