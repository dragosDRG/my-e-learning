function saveComment(userId, courseId) {
    var name = jQuery('#name').val();
    var title = jQuery('#title').val();
    var comment = jQuery('#comment').val();
    var data = {name: name, title: title, comment: comment, userId: userId, courseId: courseId};

    jQuery.ajax({
        type: "POST",
        url: '/save-comment',
        cache: false,
        data: data,
        success: function (datas) {
            alert(datas);
        }
    });
}

function like(teacherId) {
    var $data = {teacherId: teacherId};

    jQuery.ajax({
        type: "POST",
        url: '/like',
        cache: false,
        data: $data,
        success: function (data) {
            alert(data);
        }
    });
}