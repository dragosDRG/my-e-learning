function register() {
    var email = jQuery('#email').val();
    var password = jQuery('#password').val();
    var firstNames = jQuery('#firstNames').val();
    var secondName = jQuery('#secondName').val();
    var selectRole = jQuery('#selectRole').val();
    var descriere = jQuery('#descriere').val();
    var profesie = jQuery('#profesie').val();

    var $data = {
        email: email,
        password: password,
        firstNames: firstNames,
        secondName: secondName,
        selectRole: selectRole,
        descriere: descriere,
        profesie: profesie
    };

    jQuery.ajax({
        type: 'POST',
        url: '/add-new-account',
        cache: false,
        data: $data,
        success: function (data) {
            window.location.href = '/'
        }
    });
}